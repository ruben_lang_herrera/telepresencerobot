function Robot(_connection){
    this.connection = _connection;

    //gets a string message and a sender
    this.command = function(message, sender){
    //commands from arrows need to be traslated.
    //this is the place to implement any custom logic..
        translations = {
        'left':'l',
        'right':'r',
        'up': '1',
        'down': 'b'
        };
        if (translations[message])
            this.connection.send(translations[message]);
        else
            this.connection.send(message);

    };

    // handles input from the serial port
    this.response = function(message, bytes) {
        converted = String.fromCharCode.apply(String, message);
        console.log("Received " + bytes + " bytes from serial port: " + message + ", string version: " + converted);
    };
}