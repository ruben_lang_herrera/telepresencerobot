/**
 * Sends messages to the COM port, byte-by-byte. Requires a jUART browser plugin
*/


function SerialConnection(_portName, _baudRate, _pluginDivId) {
    this.portName = _portName;
    this.baudRate = _baudRate;
    this.pluginDivId = _pluginDivId

    this.port = null;

    this.robot = null; // reference to the parent robot

    this.initialize = function ()
    {
        this.port = plugin().Serial;// Get a Serial object
        this.port.open(this.portName);// Open a port
        this.port.set_option(this.baudRate,0,8,0,0);// Set port options
        this.port.recv_callback(recv); // Callback function for recieve data
    }


   this.send = function (msg, sender) {
        if(!this.port) alert("No COM port connection");
        for(var i=0;i<msg.length;++i)
        {
            this.port.send(msg[i]);
        }
    }

    function recv(bytes, size)
    {
        if(window.robot.response)
            window.robot.response(bytes, size);
    }

    this.pluginValid = function()
    {
        if(this.plugin().valid){
            alert(plugin().echo("This plugin seems to be working!"));
        } else {
            alert("Plugin is not working :(");
        }
    }
    function plugin()
    {
        return document.getElementById(this.pluginDivId);
    }
};

