
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes/routehandler')
  , http = require('http')
  , https = require('https')
  , path = require('path')
  , fs = require('fs');

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 4000);
  app.set('views', __dirname + '/views');
  app.engine('html', require('uinexpress').__express)
  app.set('view engine', 'html');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('your secret here'));
  app.use(express.session());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

keys_dir = __dirname + '/ssl-keys';
var options = {
  key: fs.readFileSync(keys_dir + '/key.pem'),
  cert: fs.readFileSync(keys_dir + '/cert.pem')
};



app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/', routes.authenticator.checkIn);
app.post('/trytolog', routes.authenticator.trytolog);
app.get('/logout', routes.authenticator.logout);


ssl_port = 4433;
https.createServer(options, app).listen(ssl_port, function(){
  console.log("Express server, ssl version listening on port " + ssl_port);
});
http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
