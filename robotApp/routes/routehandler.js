var _ = require('underscore');
var users = require('../users.json');

/************************************* vLine functions ************************************/
var jwt = require('green-jwt');
/*
 Service Name:	telepresence
 Service ID:	robots
 API Secret:	yNiNIL7LP9UBLtmSAVLD0CxQzZgD9y8R0ZO01Ww3Pl8
 Authorization:	Username/Password
 */
var serviceId = 'robots';   // replace with your service ID
var apiSecret = 'yNiNIL7LP9UBLtmSAVLD0CxQzZgD9y8R0ZO01Ww3Pl8';   // replace with your API Secret

/**
 * Create the token for a particular User.
 * @param userId
 * @returns {*} - user Token
 */
function createToken(userId) {
    var exp = new Date().getTime() + (48 * 60 * 60);     // 2 days in seconds

    return createAuthToken(serviceId, userId, exp, apiSecret);
}
/**
 * General Function for Tocken Creation.
 * @param serviceId  - Your Service ID-
 * @param userId     - Particular User Id.
 * @param expiry     - time for session expiry.
 * @param apiSecret  - YOur Api Secret.
 * @returns {*} - Enconde Token.
 */
function createAuthToken(serviceId, userId, expiry, apiSecret) {
    var subject = serviceId + ':' + userId;
    var payload = {'iss': serviceId, 'sub': subject, 'exp': expiry};
    var apiSecretKey = base64urlDecode(apiSecret);
    return jwt.encode(payload, apiSecretKey);
}
/**
 * Base 64 Enconde function. (For Token creation.)
 * @param str
 * @returns {Buffer}
 */
function base64urlDecode(str) {
    return new Buffer(base64urlUnescape(str), 'base64');
}
/**
 * Base 64 Unescape (Utils for token creation.)
 * @param str
 * @returns {XML}
 */
function base64urlUnescape(str) {
    str += Array(5 - str.length % 4).join('=');
    return str.replace(/\-/g, '+').replace(/_/g, '/');
}
/************************************* vLine functions ************************************/


exports.authenticator = {
    /**
     * Create a token for the authenticated user.
     * Render the layout if is auth
     * else Login view.
     * @param req
     * @param res
     */
    checkIn: function (req, res) {
        if (req.session.authed) {
            var vlineAuthToken = createToken(req.session.user.id);
            res.render('layout', // Layout View
                {
                    // Parameters for full fill the View.
                    ses: req.session,
                    jwt: vlineAuthToken,
                    users: users,
                    serviceId: serviceId
                }
            );
        }
        else {
            res.render('login', {layout: 'login'});
        }
    },

    /**
     * Get the login request and return auth if is correct.
     * @param req
     * @param res
     */
    trytolog: function (req, res) {
        var user = _.find(users, function (user) {
            return user.username == req.body.username && user.password == req.body.password;
        });
        if (user) {
            req.session.authed = true;
            req.session.user = user;
            res.send({authed: true});
        } else {
            res.send({authed: false});
        }

    },
    /**
     * Logout Function
     * @param req
     * @param res
     */
    logout: function (req, res) {
        if (req.session) {
            req.session.auth = null;
            res.clearCookie('auth');
            req.session.destroy(function () {
            });
        }
        res.redirect('/');
    }
};
