Telepresence robot app based on example vLine integration

Server is listening for http requests on port 4000 and for https requests on 443

Enable auto-answering:
CHROME: to use https.
FIREFOX: in "about:config" change "media.navigator.permission.disabled" to "true" (applies to all visited websites)


In order for the application to run you need to install node.js. You
can install node.js with an [installer](http://nodejs.org/download/) or [package
manager](https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager.).

## Getting Started

1. Install dependencies using the Node Package Manager (`npm`): `npm install`
2. Start the server: `npm start`
3. Open http://localhost:4000 or https://localhost in your browser
