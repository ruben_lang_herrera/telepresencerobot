/**
 * Custom code related to vLine connection
*/


/**
 * Function returning a client needed by vLine library,
 * needs to be called with server-side generated connection parameters
*/
var makeClient = function(_authToken, _serviceId, _profile) {
    var vlineClient = (function () {

        if (_serviceId == 'YOUR_SERVICE_ID') {
            alert('Please make sure you have created a vLine service and that you have properly set the serviceID and apiSecret variables on routes/routehandler.js file.');
        }

        var client, vlinesession,
                authToken = _authToken,
                serviceId = _serviceId,
                profile = _profile

        var options = {
            serviceId: serviceId,
            ui: true,
            uiVideoPanel: 'video-wrapper'
        };


        window.vlineClient = client = vline.Client.create(options);

        /**
         * Sending a msg.
         * @type {*|jQuery}
         */
        /*  var msg =  $('#msg').val();
         $('#sendMsg').click(sendMsg(client,msg));*/

        // Add login event handler
        client.on('login', onLogin);

        /**
         * Add Message Event Handler
         */
        client.on('recv:im', onMsg, this);


        client.login(serviceId, profile, authToken);

        function onMsg(event) {
            var msg = event.message;
            var sender = msg.getSender();
            console.log('From:' + sender.getDisplayName() + ' message: ' + msg.getBody());

            var newMsg = '<li class="msg-item">' +
                            '<p class="pull-left"><i class="icon-user icon-white"></i>&nbsp;'+sender.getDisplayName()+':</p>'+
                            '<p class="pull-left msg-body">'+msg.getBody()+'</p>'+
                            '<div class="clearfix"></div>'+
                          '</li>';

            $('.msg-list').prepend(newMsg);
        }

        function onLogin(event) {
            vlinesession = event.target;

            // Find and init call buttons and init them
            $(".callbutton").each(function (index, element) {
                initCallButton($(this));
            });
            // Init Send Message function.
            initSendMessage(vlinesession);
            initControlBox(vlinesession);
        }

        /**
         * Init Call User Function.
         * @param button - User List Item.
         */
        function initCallButton(button) {
            var userId = button.attr('data-userid');

            // fetch person object associated with username
            vlinesession.getPerson(userId).done(function (person) {
                // update button state with presence
                function onPresenceChange() {
                    if (person.getPresenceState() == 'online') {
                        var presence = button.find('.presence');
                        presence.addClass('online');
                        button.removeClass().addClass('isActive');
                    } else {
                        button.removeClass();
                        var presence = button.find('.presence');
                        presence.removeClass('online');
                    }
                    button.attr('data-presence', person.getPresenceState());
                }

                // set current presence
                onPresenceChange();

                // handle presence changes
                person.on('change', onPresenceChange);

                // start a call when button is clicked
                button.click(function () {
                    if (person.getId() == vlinesession.getLocalPersonId()) {
                        alert('You cannot call yourself. Login as another user in an incognito window');
                        return;
                    }
                    if (button.hasClass('isActive')) {
                        window.lastUserSelected=person;
                        person.startMedia();
                    }
                });
            });
        }

        /**
         * Init the Send Message Function.
         * @param vSession
         */
        function initSendMessage(vSession){
            $('#sendMsg').click(function(){
                if(window.lastUserSelected){
                    var msg = $('#msg').val();
                    postMsg(vSession,window.lastUserSelected,msg);
                    $('#msg').val("");
                }else{
                    alert('Recipient of the message not specified. Please click on the user name');
                }
            });
        }

        /**
         * Initialize the move box.
         * @param vSession
         */
        function initControlBox(vSession){

              $('.move-box .btn').each(function(){
                  var btn =  $(this);
                  var action =  $(this).attr('data-action');
                  btn.click(function(){
                      console.log("Command: "+action);
                      postMsg(vSession,window.lastUserSelected,action);
                  });
              });
        }

        /**
         * Post a message.
         */
        function postMsg(session,person,msg){
            session.postMessage(person.getId(), msg);
            console.log('to: '+person.getId()+' >> '+msg);
        }

        return client;
    })();
    return vlineClient;
}

$(window).unload(function () {
    vlineClient.logout();
});