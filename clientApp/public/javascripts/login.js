/**
 * Created with JetBrains WebStorm.
 * User: ruben
 * Date: 10/12/13
 * Time: 22:21
 * To change this template use File | Settings | File Templates.
 */

function trytolog() {
    $.ajax({
        url: '/trytolog',
        type: 'POST',
        contentType: "application/json",
        async: true,
        data: JSON.stringify({username: $("#username").val(), password: $("#password").val()}),
        success: function (data) {
            if (data.authed) {
                window.location = "./";
            } else {
                $(".authfailure").show();
            }
        }
    });
}
